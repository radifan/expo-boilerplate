/* eslint-disable no-undef */

import Expo from 'expo';
import './app/config/reactotronConfig';

const Entrypoint = require('./app/screens/app').default;

Expo.registerRootComponent(Entrypoint);
