import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import ReduxNavigation from 'navigation/reduxNavigation';

// Styles
import styles from './styles';

class RootScreen extends Component { // eslint-disable-line
  render() {
    return (
      <View style={styles.container}>
        <ReduxNavigation />
      </View>
    );
  }
}

RootScreen.propTypes = {
  // startup: PropTypes.func,
};

// wraps dispatch to create nicer functions to call within our component
const mapDispatchToProps = (dispatch) => ({
  dispatch,
});

export default connect(null, mapDispatchToProps)(RootScreen);
