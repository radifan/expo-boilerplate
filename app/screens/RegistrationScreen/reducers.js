import { LOAD_REGISTER } from './constants';

const initialState = {
  loading: false,
  errors: false,
};

function registrationReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_REGISTER:
      return state;
    default:
      return state;
  }
}

export default registrationReducer;
