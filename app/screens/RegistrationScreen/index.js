import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Keyboard, View } from 'react-native';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withFormik } from 'formik';
import Yup from 'yup';
import FullScreenLayout from 'layouts/FullScreenLayout';
import { Input, Button, Text } from 'components/Common';
import injectReducer from 'utils/reduxHelper/injectReducer';
import injectSaga from 'utils/reduxHelper/injectSaga';
import { setToken } from 'store/auth/actions';

import { loadRegister } from './actions';
import reducer from './reducers';
import saga from './saga';
// import styles from './styles';

class RegistrationScreen extends Component { // eslint-disable-line
  componentDidMount() {
    this.props.dispatch(loadRegister());
  }

  render() {
    const { setFieldValue, handleSubmit } = this.props;

    return (
      <FullScreenLayout>
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <Text as="h1" textAlign="center">
            LOGIN
          </Text>
          <Input label="Username" name="username" onChangeText={setFieldValue} />
          <Input label="Password" name="password" onChangeText={setFieldValue} password />
          <Button title="SUBMIT" onPress={handleSubmit} />
        </View>
      </FullScreenLayout>
    );
  }
}

RegistrationScreen.propTypes = {
  setFieldValue: PropTypes.func,
  handleSubmit: PropTypes.func,
  dispatch: PropTypes.func,
};

const EnhancedForm = withFormik({
  mapPropsToValues: () => ({
    username: '',
    password: '',
  }),
  validationSchema: Yup.object().shape({
    username: Yup.string()
      .required('Username is required!'),
    password: Yup.string()
      .required('Password is required!'),
  }),
  handleSubmit: (values, { props, setSubmitting }) => {
    props.dispatch(setToken(values.username));
    Keyboard.dismiss();
    setSubmitting(false);
  },
  displayName: 'LoginForm',
});

function mapStateToProps(state) {
  return {
    registrationContainer: state.registrationContainer,
    nav: state.nav,
    auth: state.auth,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withReducer = injectReducer({ key: 'registrationContainer', reducer });
const withSaga = injectSaga({ key: 'registrationContainer', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)((EnhancedForm)(RegistrationScreen));
