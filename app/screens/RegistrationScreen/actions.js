import { LOAD_REGISTER } from './constants';

export function loadRegister() {
  return {
    type: LOAD_REGISTER,
  };
}
