import { takeLatest } from 'redux-saga/effects';
import { LOAD_REGISTER } from './constants';

export function* loadRegisterFlow() {
  console.log('Load register flow');
}

export default function* defaultSaga() {
  yield takeLatest(LOAD_REGISTER, loadRegisterFlow);
}
