import React from 'react';
// ismport PropTypes from 'prop-types';
import FullScreenLayout from 'layouts/FullScreenLayout';
import { Text } from 'components/Common';

class WelcomeScreen extends React.Component {
  render() {
    return (
      <FullScreenLayout>
        <Text as="h1" textAlign="center">
          Test
        </Text>
      </FullScreenLayout>
    );
  }
}

WelcomeScreen.propTypes = {

};

export default WelcomeScreen;
