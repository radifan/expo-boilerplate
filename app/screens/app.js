import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { Font, AppLoading, KeepAwake } from 'expo';
import { PersistGate } from 'redux-persist/integration/react';
import DebugConfig from 'config/debugConfig';
import configureStore from 'store/configureStore';
import { createAPI, responseInterceptor } from 'utils/api';
import 'config';

import RootScreen from './RootScreen';

// create our store
const initialState = {};
const { store, persistor } = configureStore(initialState);

// create API instance
createAPI('http://localhost:8000');
responseInterceptor(store);

/**
 * Provides an entry point into our application.  Both index.ios.js and index.android.js
 * call this component first.
 *
 * We create our Redux store here, put it into a provider and then bring in our
 * RootContainer.
 *
 * We separate like this to play nice with React Native's hot reloading.
 */
class App extends Component { // eslint-disable-line react/prefer-stateless-function
  state = {
    fontLoaded: false,
  };

  async componentWillMount() {
    await Font.loadAsync({
      'Montserrat': require('assets/Montserrat-Regular.ttf'),
      'Montserrat_semibold': require('assets/Montserrat-SemiBold.ttf'),
      'OpenSans': require('assets/OpenSans-Regular.ttf'),
    });

    await this.setState({
      fontLoaded: true,
    });
  }

  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={<AppLoading />} persistor={persistor}>
          { DebugConfig.keepAwakeScreen && <KeepAwake /> }
          { this.state.fontLoaded ? <RootScreen /> : <AppLoading /> }
        </PersistGate>
      </Provider>
    );
  }
}

// allow reactotron overlay for fast design in dev mode
export default DebugConfig.useReactotron
  ? console.tron.overlay(App)
  : App;
