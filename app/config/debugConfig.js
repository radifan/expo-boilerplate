export default {
  showDevScreens: __DEV__,
  useFixtures: false,
  ezLogin: false,
  keepAwakeScreen: __DEV__,
  yellowBox: __DEV__,
  reduxLogging: __DEV__,
  includeExamples: __DEV__,
  useReactotron: __DEV__,
  launchStorybook: false,
};
