import React from 'react';
import PropTypes from 'prop-types';
import { StatusBar, SafeAreaView } from 'react-native';
import NavigationHeader from 'components/NavigationHeader';

const DefaultLayout = ({ children, headerTitle }) => (
  <SafeAreaView>
    <NavigationHeader title={headerTitle} />
    <StatusBar barStyle="dark-content" />
    {children}
  </SafeAreaView>
);

DefaultLayout.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  headerTitle: PropTypes.string,
};

export default DefaultLayout;
