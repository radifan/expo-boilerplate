import React from 'react';
import PropTypes from 'prop-types';
import { SafeAreaView, View } from 'react-native';
import createStyles from './styles';

const FullScreenLayout = ({ children, backgroundColor }) => {
  const styles = createStyles({ backgroundColor });

  return (
    <SafeAreaView style={styles.safeAreaStyle}>
      <View style={styles.container}>
        {children}
      </View>
    </SafeAreaView>
  );
};

FullScreenLayout.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  backgroundColor: PropTypes.string,
};

export default FullScreenLayout;
