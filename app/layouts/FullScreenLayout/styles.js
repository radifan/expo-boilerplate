import { StyleSheet } from 'react-native';
import { Metrics, Colors } from 'themes';

export default ({ backgroundColor = Colors.background }) => StyleSheet.create({
  safeAreaStyle: {
    flex: 1,
    backgroundColor,
  },
  container: {
    flex: 1,
    padding: Metrics.baseMargin,
    paddingLeft: Metrics.baseMargin,
    backgroundColor,
  },
});
