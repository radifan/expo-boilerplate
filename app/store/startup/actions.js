import {
  STARTUP,
} from './constants';

export function startup() {
  return {
    type: STARTUP,
  };
}
