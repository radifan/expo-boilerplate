import {
  STARTUP,
} from './constants';

const initialState = {};

function startupReducer(state = initialState, action) {
  switch (action.type) {
    case STARTUP:
      return state;
    default:
      return state;
  }
}

export default startupReducer;
