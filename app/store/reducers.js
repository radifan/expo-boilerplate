/**
 * Combine all reducers in this file and export the combined reducers.
 */

import { combineReducers } from 'redux';
import createSecureStore from 'redux-persist-expo-securestore';
import { persistReducer } from 'redux-persist';

import { reducer as navigationReducer } from './navigation/reducer';
import authReducer from './auth/reducer';

/**
 * Creates the main reducer with the dynamically injected ones
 */
export default function createReducer(injectedReducers) {
  const secureStorage = createSecureStore();
  const authPersistConfig = {
    key: 'auth',
    storage: secureStorage,
  };

  return combineReducers({
    auth: persistReducer(authPersistConfig, authReducer),
    nav: navigationReducer,
    ...injectedReducers,
  });
}
