import { REHYDRATE } from 'redux-persist';
import {
  SET_TOKEN,
  CLEAR_TOKEN,
  LOAD_PROFILE,
  LOAD_PROFILE_SUCCESS,
  LOAD_PROFILE_FAILED,
} from './constants';

const initialState = {
  token: false,
  refreshToken: false,
  data: {
    userProfile: false,
    error: false,
    loading: false,
  },
};

function authReducer(state = initialState, action) {
  switch (action.type) {
    case SET_TOKEN:
      return Object.assign({}, state, {
        token: action.token,
      });
    case CLEAR_TOKEN:
      return Object.assign({}, state, state);
    case LOAD_PROFILE:
      return Object.assign({}, state, {
        data: {
          userProfile: false,
          loading: true,
          error: false,
        },
      });
    case LOAD_PROFILE_SUCCESS:
      return Object.assign({}, state, {
        data: {
          userProfile: action.userProfile,
          loading: false,
          error: false,
        },
      });
    case LOAD_PROFILE_FAILED:
      return Object.assign({}, state, {
        data: {
          userProfile: false,
          loading: false,
          error: action.error,
        },
      });
    case REHYDRATE:
      return Object.assign({}, state, action.payload);
    default:
      return state;
  }
}

export default authReducer;
