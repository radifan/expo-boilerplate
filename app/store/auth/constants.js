export const SET_TOKEN = 'App/Redux/Auth/SET_TOKEN';
export const CLEAR_TOKEN = 'App/Redux/Auth/CLEAR_TOKEN';
export const LOAD_PROFILE = 'App/Redux/Auth/LOAD_PROFILE';
export const LOAD_PROFILE_SUCCESS = 'App/Redux/Auth/LOAD_PROFILE_SUCCESS';
export const LOAD_PROFILE_FAILED = 'App/Redux/Auth/LOAD_PROFILE_FAILED';
