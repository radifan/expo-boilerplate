import { StackNavigator } from 'react-navigation';
import RegistrationScreen from 'screens/RegistrationScreen';
import WelcomeScreen from 'screens/WelcomeScreen';

// Manifest of possible screens
const PrimaryNav = StackNavigator({
  RegistrationScreen: { screen: RegistrationScreen },
  WelcomeScreen: { screen: WelcomeScreen },
}, {
  // Default config for all screens
  headerMode: 'none',
  initialRouteName: 'RegistrationScreen',
});

export default PrimaryNav;
