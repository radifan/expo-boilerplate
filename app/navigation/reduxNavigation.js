import React from 'react';
import { addNavigationHelpers } from 'react-navigation';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { addListener } from 'utils/reactNavigationHelper';
import AppNavigation from './appNavigation';

// here is our redux-aware smart component
function ReduxNavigation(props) {
  const { dispatch, nav } = props;
  const navigation = addNavigationHelpers({
    dispatch,
    state: nav,
    addListener,
  });

  return <AppNavigation navigation={navigation} />;
}

ReduxNavigation.propTypes = {
  dispatch: PropTypes.func,
  nav: PropTypes.object,
};

const mapStateToProps = (state) => ({ nav: state.nav });
export default connect(mapStateToProps)(ReduxNavigation);
