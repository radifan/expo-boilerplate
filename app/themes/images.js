/* eslint-disable global-require */

const images = {
  logo: require('../images/ir.png'),
  clearLogo: require('../images/top_logo.png'),
  launch: require('../images/launch-icon.png'),
  ready: require('../images/your-app.png'),
  ignite: require('../images/ignite_logo.png'),
  igniteClear: require('../images/ignite-logo-transparent.png'),
  tileBg: require('../images/tile_bg.png'),
  background: require('../images/BG.png'),
  buttonBackground: require('../images/button-bg.png'), // eslint-disable-line import/no-unresolved
  api: require('../images/icons/icon-api-testing.png'), // eslint-disable-line import/no-unresolved
  components: require('../images/icons/icon-components.png'), // eslint-disable-line import/no-unresolved
  deviceInfo: require('../images/icons/icon-device-information.png'), // eslint-disable-line import/no-unresolved
  faq: require('../images/icons/faq-icon.png'), // eslint-disable-line import/no-unresolved
  home: require('../images/icons/icon-home.png'), // eslint-disable-line import/no-unresolved
  theme: require('../images/icons/icon-theme.png'), // eslint-disable-line import/no-unresolved
  usageExamples: require('../images/icons/icon-usage-examples.png'), // eslint-disable-line import/no-unresolved
  chevronRight: require('../images/icons/chevron-right.png'), // eslint-disable-line import/no-unresolved
  hamburger: require('../images/icons/hamburger.png'), // eslint-disable-line import/no-unresolved
  backButton: require('../images/icons/back-button.png'), // eslint-disable-line import/no-unresolved
  closeButton: require('../images/icons/close-button.png'),
};

export default images;
