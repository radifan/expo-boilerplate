const colors = {
  background: '#fbfbff',
  clear: 'rgba(0,0,0,0)',
  transparent: 'rgba(0,0,0,0)',
  primary: '#0b4f6c',
  secondary: '#01baef',
  gray: '#757575',
  green: '#20bf55',
};

export default colors;
