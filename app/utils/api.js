import axios from 'axios';

let api;

export function createAPI(baseURL) {
  api = axios.create({
    baseURL,
    timeout: 1000,
  });
}

export function setAuthorizationHeader(token) {
  api.defaults.headers.common.Authorization = `Bearer ${token}`;
}

export function removeAuthorizationHeader() {
  delete api.defaults.headers.common.Authorization;
}

export function responseInterceptor(store) {
  return api.interceptors.response.use((response) => response, (error) => {
    switch (error.response.status) {
      case 401:
        return store.dispatch();
      case 404:
        return store.dispatch();
      case 500:
        return store.dispatch();
      default:
        return error;
    }
  });
}

export default (config) => api.request(config);
