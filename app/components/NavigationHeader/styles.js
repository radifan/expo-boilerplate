import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    height: 60,
  },
  centerHeader: {
    justifyContent: 'center',
  },
});
