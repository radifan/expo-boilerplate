import React from 'react';
import PropTypes from 'prop-types';
import { View, Text } from 'react-native';
import styles from './styles';

const NavigationHeader = ({ title }) => (
  <View style={styles.container}>
    <Text>
      {title || 'No Title'}
    </Text>
  </View>
);

NavigationHeader.propTypes = {
  title: PropTypes.string,
};

export default NavigationHeader;

