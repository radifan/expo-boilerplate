import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  titleStyle: {
    fontFamily: 'Montserrat_semibold',
    fontSize: 20,
    color: '#ffffff',
  },
});
