import React from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity } from 'react-native';
import { Button as RNEButton } from 'react-native-elements';
import styles from './styles';

const Button = ({
  title, onPress, noTopMargin, loading, disabled, primary,
}) => {
  const linearGradient = {
    colors: ['#CFCFCF', '#D6D6D6'],
    start: [0.0, 0.5],
    end: [1, 0.5],
  };
  if (primary) {
    linearGradient.colors = ['#01BAEF', '#74E0FF'];
  }

  return (
    <RNEButton
      title={title}
      onPress={onPress}
      titleProps={{ style: styles.titleStyle }}
      linearGradientProps={linearGradient}
      TouchableComponent={TouchableOpacity}
      buttonStyle={{
        marginTop: noTopMargin ? 0 : 20,
        height: 60,
        elevation: 0,
        borderRadius: 50,
      }}
      disabledStyle={{
        opacity: 0.2,
      }}
      loading={loading}
      disabled={loading || disabled}
    />
  );
};

Button.propTypes = {
  title: PropTypes.string.isRequired,
  onPress: PropTypes.func,
  noTopMargin: PropTypes.bool,
  loading: PropTypes.bool,
  disabled: PropTypes.bool,
  primary: PropTypes.bool,
};

export default Button;
