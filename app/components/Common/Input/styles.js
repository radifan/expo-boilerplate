import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  containerStyle: {
    width: '100%',
    borderBottomWidth: 0.3,
    borderBottomColor: '#e2e2e2',
    marginBottom: 10,
  },
  inputContainerStyle: {
    borderBottomWidth: 0,
    borderColor: 'transparent',
  },
});
