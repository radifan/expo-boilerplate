import React from 'react';
import PropTypes from 'prop-types';
import { Input as RNEInput } from 'react-native-elements';
import styles from './styles';

const Input = ({
  label, name, password, onChangeText,
}) => (
  <RNEInput containerStyle={styles.containerStyle} inputContainerStyle={styles.inputContainerStyle} placeholder={label} name={name} onChangeText={onChangeText} secureTextEntry={password} />
);

Input.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string,
  password: PropTypes.bool,
  onChangeText: PropTypes.func,
};

export default Input;
