import React from 'react';
import PropTypes from 'prop-types';
import { Text as RNText } from 'react-native';
import styles from './styles';

const Text = ({
  children, as = 'normal', textAlign = 'left', style, noBottomMargin,
}) => (
  <RNText style={[styles[as], { textAlign, marginBottom: noBottomMargin ? 0 : 20 }, style]}>
    {children}
  </RNText>
);

Text.propTypes = {
  as: PropTypes.oneOf([
    'h1',
    'h2',
    'h3',
    'h4',
    'h5',
    'h6',
    'normal',
    'medium',
  ]),
  textAlign: PropTypes.oneOf([
    'center',
    'left',
    'right',
  ]),
  noBottomMargin: PropTypes.bool,
  children: PropTypes.node,
  style: PropTypes.object,
};

export default Text;
